<?php
/**
 * Chargement du plugin Ueno
 *
 * @plugin     Ueno
 * @copyright  2019-2022
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Ueno\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
$GLOBALS['fin_intertitre'] = "\n</h2>\n";

// pagination : 6 liens max
define('_PAGINATION_NOMBRE_LIENS_MAX', 7);